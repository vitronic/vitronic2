------------------------------------------------------------------------------
--
--  Funciones comunes de Vitronic.
--
--  Copyright (c) 2019 Máster Vitronic
--  Licensed under the GPL license version 2:
--  https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
--
--  @autor Díaz Devera Víctor Diex Gamar <vitronic2@gmail.com>
--  @date  Wed Mar 20 06:41:56 -04 2019
------------------------------------------------------------------------------

local common    =   class("common")

-- Convierte de formato moneda a numerico
-- @param   string : 15.513,65
-- @return  decimal: 15513.65
function common:money2number(money)

end

-- Convierte de formato numerico a moneda
-- @param   decimal: 15513.65
-- @return  string : 15.513,65
function common:number2money(number)

end

return common
