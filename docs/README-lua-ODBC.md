[README.md](README.md)

# ODBC EN general

*"Open Database Connectivity"* `ODBC`, es una especificacion abierta 
para llmadas SQL desde otros lenguajes de programacion y **en lua es solo 
una capa de manejo de las llamadas** por lo que no espere resultados especiales.
**Esto significa que es mejor hacer la mayor parte en codigo que con trucos sql**.
que es lo mismo que por ejemplo no iterar cursores sino iterar arreglos, 
los cuales ya tienen el resultado de los cursores.

La teoria completa sobre ODBC en linux se puede ver en este articulo:
http://gambaswiki.org/wiki/doc/odbc?l=es

## justificacion y ventajas

**¿Si es tan limitado entonces cual es su ventaja y porque existe?**

Bases de datos como sqlite y mysql estan bien para cafeterias pero 
si se quiere interconectar con sistemas profesionales o en produccion, 
estos usan **DBMS como Sybase, Informix, DB2, Firebird, SQLserver y Oracle**, 
por lo que un ODBC **FreeTDS** o **Easysoft** es necesario. Este ultimo 
es el mayor proveedor de "modulos" ODBC para Linux/unix pero codigo cerrado.

## ODBC infraestructura de uso

El sistema ODBC consta de 3 partes, el manager (unixodbc), un 
modulo odbcinst de conectividad y una defincion DSN odbc.

LA instalacion es primero UnixODBC, despues cualquier modulo. Entre los 
modulos mas usados estan FreeTDS, MySQL-conector y los de Easysoft.

* Debian: `apt-get install unixodbc tdsodbc libmyodbc odbc-postgresql libsqlite3-mod-csvtable,libsqliteodbc`
* Alpine: `apk add unixodbc freetds mysql-connector-odbc psqlodbc sqliteodbc`
* Otros: en la mayoria de linux sera necesario compilar a mano, especialmenre DB2/firebird.

### El modulo ODBC inst

Para trabajar con una conexcion DNS se requiere el "driver" modulo ODBC 
sea mysql-connector, freetds etc, al instalar el tipo de conector este 
se registra en `/etc/odbcinst.ini` para poder usarse como librerias de conectividad. 

Cada modulo es una seccion en est archivo. Este puede cambiarse o usarse 
uno alterno al del sistema por medio de la variable `ODBCINST` con "export", 
si la variable no esta definida o se define a vacio se asume en `/etc/odbcinst.ini`
quedando asi aproximadamente:

``` ini
  
[FreeTDS]
Driver		= /usr/lib/odbc/libtdsodbc.so
Setup		= /usr/lib/odbc/libtdsS.so
UsageCount	= 1

[MySQL]
Driver		= /usr/lib/odbc/libmysqlodbc.so
Setup		= /usr/lib/odbc/libmysqlS.so
UsageCount	= 1
```

**IMPORTANTE** las rutas de "Driver" dependen de la version de linux/instalacion

### El DSN y odbc.ini

El DSN es el nombre de la conexcion, y se guarda en archivos "odbc.ini" 
cada conexcion es uan seccion del archivo.
Se usa la libreria de acceso ejemplo `FreeTDS` registrada en el `/etc/odbcinst.ini` 
para la conectividad del DSN, en el archivo `/etc/odbc.ini` con los parametros de 
conexcion a la base de datos.

En linux hay dos alcances de un DSN, por sistema o por usuario:

* si configura el DSN en `/etc/odbc.ini` lo vera todo el mundo.
* si configure el DSN en `~/.odbc.ini` lo vera solo el usuario.

**NOTA:** no hay usuarios para servicios, ejemplo en Debian el usuario 
del webserver es `www-data` y en Alpine es `www` ningun usuario de servicos 
tendra un home por seguridad, por lo que no existe lugar para el "odbc.ini". 
para esto el ultimo unixodbc provee el "connection string" que se define 
toda la seccion de conexion de odbc.ini en una sola linea en el codigo.

Se muestra coneciones a db local en MySQL/MAriaDB, a MSSQL y a Sybase:

``` ini
[dblocal]
Driver		= MySQL
Database	= mysql
Server		= localhost
Port		= 2638
ReadOnly	= No

[oasisdb]
Driver		= FreeTDS
Database	= sybasedemo
Server		= 10.10.200.10
Port		= 2638
ReadOnly	= No
TDS_Version	= 5.0

[saint]
Driver			= FreeTDS
Database		= SWNOMMSSQL
Server			= 192.168.1.100
Port			= 1723
ReadOnly		= Yes
TDS_Version		= 8.0
```

### Probando en ODBC configurado

El paquete UnixODBC tiene un programa llamado `isql` el cual se puede usar para probar, debe 
usarse desde el contexto del usuario, es decir si se configura en un "home" se debe hacer la 
llamada del programa desde el usuario y no desde otro usuario o administrador.

**IMPORTANTE** Las credenciales se deben pasar, no toma las del DSN configurado.

``` bash

% isql saint sa saclave
+---------------------------------------+
| Connected!                            |
|                                       |
| sql-statement                         |
| help [tablename]                      |
| quit                                  |
|                                       |
+---------------------------------------+
SQL>select cedula, apenom from swnomper
```

# Usando ODBC en lua

Para usar una conexion en lua del tipo ODBC primero se necesita tener funcionando 
el unixodbc y el conector/driver odbc de la db a la cual conectar, despues 
se define el DSN usando este segundo y lo siguiente ya es igual que cualquier 
otro uso de base de datos (si se usase lua-sql).

Aunque se usase otros capas para lua, el unixodbc, el odbcinst y el DSN 
deberan siempre estar primero antes de poder implementar codigo alguno.

El DSN es la definicion de conexcion que se usar realmente en el codigo fuente.

### Usando lua-sql

Este se recomienda en proyectos donde existan dudas sobre el uso, y 
asi puede cambiar de manejador de DBMS a uno espcifico, ya que el API 
y el codigo fuente es igual si se alterna entre ODBC o cualqueir otro.

WIP

### Usando lua-odbc

Este es el recomendado si siempre usara ODBC sin importar que, la unica ventaja 
de lua-sql sobre lua-odbc es que con cambiar dos simples lineas se podra 
corroborar tanto si la sentencia es correcta asi como si el resultado devuelto 
es correcto por el transporte de la capa odbc del manejador.

```
cat > /etc/odbc.ini << EOF
[ODBC]
Driver		= /usr/lib/odbc/libtdsodbc.so
Setup		= /usr/lib/odbc/libtdsS.so
UsageCount	= 1

[saint]
Driver		= /usr/lib/odbc/libtdsodbc.so
Database	= SWNOMMSSQL
Server		= 192.168.1.100
Port		= 1723
ReadOnly	= Yes
TDS_Version	= 8.0
EOF

cat > ejemplo.lua << EOF
odbc = require "odbc"
dbassert = odbc.assert
 
cnn = odbc.connect('saint', 'sa', 'saclave')
 
stmt = cnn:execute('select cedula, apenom from swnomper')
stmt:foreach(function(f1, f2)
  print(string.format("Cedula numero %d, su nombre es: %s", f1, f2));
 
end)
assert(stmt:closed()) -- foreach close cursor
assert(not stmt:destroyed()) -- statement valid
EOF

lua ejemplo.lua
```
