
## Requerimeintos

## Comenzar:

Si solo quiere probar el proyecto puede clonar directo y compilar e instalar.
OJO: si quiere desarrollar y contribuir debe leer [flujo](#flujo).

```
export https_proxy=http://192.168.1.104:8123
git clone https://gitlab.com/vitronic/vitronic.git
cd vitronic
make
./vitronic
```

## FLUJO:

Cada feature o trabajo de cada desarrollador debe tener un branch o rama 
y este debe estar en su propio fork o bifurcacion (como clonar a su usuario en gitlab)
este al clonar en la consola debe trabajar alli asi el merge/pull 
sera unisono de solo el trabajo que realize.
Dato especial es cuadno su trabajo esta sobre codigo desactualizado 
entonces realiza un rebase de su codigo apra sincronizar.


Como ejemplo @PICCORO trabaja en la rama `docs-main-direct` para este 
tipo de documentacion y el proceso es asi:
* Visitar el repo "vitronic" y pulsar "fork" al usuario "mckaygerhard"
* Una vez bifurcado crear la rama a trabajar "docs-main-direct"
* clonar el repo "personal" y cambiar a la rama trabajar:
```
export https_proxy=http://192.168.1.104:8123
git clone https://gitlab.com/mckaygerhard/vitronic.git
git checkout -b docs-main-direct origin/docs-main-direct
```
* Hacer commit y push hacia el preo personal o bifurcado
* Realizar un pull request o merge request desde su repo con "Compare"

Esto creara un issue merge request que una vez visto se aprobara, 
terminado y "merge" (acometido y mexclado) en el repo "vitronic" 
puede eliminarse la rama o seguir trabajando en ella (recomendado asi).

