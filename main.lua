#!/usr/bin/env lua5.1

--[[!
@package   Vitronic
@filename  main.lua
@version   1.0
@autor     Díaz Devera Víctor Diex Gamar <vitronic2@gmail.com>
@date      Sat Mar 16 18:16:36 -04 2019
]]--

--La libreria middleclass para dotarnos de OOP
require('lib.middleclass')
--En lib/funciones guardare todas las funciones generales
funcion         = require('lib.funciones')
-- Similar a funciones pero mas comun
comun           = require('lib.comun')
-- La libreria que me permitira usar GTK
local lgi       = require('lgi')
-- Parte de lgi
local GObject   = lgi.GObject
-- para el treeview
local GLib      = lgi.GLib
-- El objeto GTK
local Gtk     = lgi.require('Gtk', '3.0')
--local Gtk       = lgi.require('Gtk')
--Esto es Gettext señores
local Gettext   = require("lib.gettext")
--Inicializo mis locales (luego jalar esto de la DB)
local fd,err=io.open("po/es.mo","rb")
if not fd then return nil,err end
local raw_mo=fd:read("*all")
fd:close()
gettext         = Gettext:gettext(raw_mo)
_               = gettext

local assert    = lgi.assert
local builder   = Gtk.Builder()

assert(builder:add_from_file('views/main.glade'))
local ui        = builder.objects

--la ventana principal
local main_window   = ui.main_window

--print(GLib.get_home_dir())          --el home
--print(GLib.get_current_dir())       --el directorio actual
--print(GLib.get_user_config_dir ())  --el directorio de configuracion
--print(GLib.get_user_data_dir())     --el directorio de datos de la aplicacion
--print(GLib.random_int())            --generar un random
--print((GLib.get_real_time())        --unix microsegundos

ui.title.label = os.date("%H:%M:%S")
GLib.timeout_add_seconds(
    GLib.PRIORITY_DEFAULT, 1,
    function()
        ui.title.label = os.date("%H:%M:%S")
        return true
    end
)

--Bueno asi se usaria el gettext
print(gettext("Some information of this account has been updated"))
print(_("Some information of this account has been updated"))

--Ojo que esto es opcional, pero no esta mal un incon en el panel
local visible   = false
function showHide()
	visible = not visible
	if visible then
		main_window:show_all()
	else
		main_window:hide()
	end
end
status = Gtk.StatusIcon()
status:set_from_icon_name('media-eject')
status:set_visible(true)

--al hacer click sobre el status_icom
function status:on_activate()
	showHide()
	--menu:show_all()
end

--Al hacer click derecho sobre el status_icom
function status:on_popup_menu()
    print('falta mostrar un menu conextual aqui')
    --menu:popup(menu,nil,nil,status.position_menu )
end

--que hacer cuando cierrenla ventana principal
function main_window:on_destroy()
    Gtk.main_quit()
end

--Conecto el menu about a el win_dialogo about
function ui.menu_about:on_clicked()
    ui.aboutdialog.version  = '0.1-alpha' --Aqui la version
    ui.aboutdialog:run()
    ui.aboutdialog:hide()
end

--cuando se presione Quit
function ui.menu_quit:on_clicked()
    Gtk.main_quit()
end

-- Show window and start the loop.
main_window:show_all()
Gtk.main()






