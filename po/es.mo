��    n      �  �   �      P	  
   Q	     \	     e	     m	     v	     �	     �	     �	     �	     �	     �	     �	  	   �	  	   �	     �	     �	      
     
     
  9   .
     h
     u
     �
     �
     �
     �
     �
     �
     �
  _   �
     2     9     @  	   Q     [     b     g     o     t     {     ~  	   �  	   �     �     �     �     �     �     �  J   �          #     ,     5  
   =     H  	   L     V     \  
   a     l     �     �     �     �     �     �     �  
   �     �     �     �     �                    /     F     S  6   d  =   �  (   �  B        E     c     l  -   u  1   �     �     �  +   �          %  
   .     9     @  B   S  G   �     �  	   �  &   �                          '     -     K     P  q  U     �     �     �  	   �     �                    (     :     Q     ]  	   j     t     }     �     �     �  "   �  K   �          $     1     E     L     l     y  
   �     �  k   �                    4     D     K  	   Q     [     b     k     n  
   |     �  
   �     �  	   �     �  
   �     �  X   �     5     H  
   P     [  	   d     n     t     �     �     �  (   �     �     �     �     �  	   �                    #     +     4     J     Q     c     r  "   �     �     �  8   �  <   
  /   G  M   w  (   �     �     �  (     ;   6     r     y  '   �     �     �  	   �     �  	   �  F   �  F   +     r     u  )   z     �     �     �     �     �     �     �     �     G   3   &   n      O                        m                          E   `   [          ,           :          _       !   .   Q   8      	       A   #   /   F   j                 J   
       ;      c   '   "   U       $       -         P       l          M   I                  S   D                     C   a   b   h   )   T   W      6   f       e       1         9   4      0      >   @   7   K   <   N   \   Y       X   5   %   2   g   ^          ]          *       Z   =              V   d   L              (          R      +       H       k   i   B   ?    Accounting Accounts Actions Activity Add a profile picture. Affected Accounts All Assignee Attachments Belonging to the Group Budget Budgets Campaigns Catalogue Charges Clone Closed Configuration Confirm access password. Contact your administrator if you think this is an error. Created date Credentials Custom Filter Delete Describe the issue in detail Disable Due date Earrings Edit Enter password, at least 8 alphanumeric characters with at least one uppercase are recommended. Expand Export Full Description Full name Groups Help History Home Import In Integrations Inventory Invoicing Issue Issues Labels Last update Leads Logout Make sure the address is correct and that you have the proper permissions. Moments ago My Cloud My board My menu My profile New New Issue Nodes Open Operations Optionally add a due date. Override Payments Phone Position and size avatar Priority Profile Profiles Promotions Report Reports Resource not found Sales Search Issue Search User Seconds from now Select Account Status. Select Image Select Milestone Select one or more accounts if the issue affects them. Select one or more users to assign this request, you can also Select the group the account belongs to. Select the tags that best describe the situation of this incident. Select weight for this issue. Settings Sign out Sign out and sign in with a different account Some information of this account has been updated Tasks The title of the issue. This account has been successfully imported Title Tomorrow Unfeasible Weight Without milestones Write down the real name so that people you know can recognize it. Write the user name, it will be the credential of access to the system. YES Yesterday You can add a milestone to this issue. ago day hour minute month translated %s with parameters week year Project-Id-Version: IspCore 1.0
Report-Msgid-Bugs-To: <vitronic2@gmail.com>
POT-Creation-Date: 2018-09-26 17:50+0400
PO-Revision-Date: 2018-09-26 17:13+0400
Last-Translator: Diaz Devera Victor <vitronic2@gmail.com>
Language-Team: es_ES.UTF-8 <vitronic@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Contable Cuentas Acciones Actividad Añada una imagen de perfil Cuentas afectadas Todos Asignado Archivos adjuntos Perteneciente al Grupo Cotización Cotizaciones Campañas Catalogo Cobros Clonar Cerrado Configuración Confirme la contraseña de acceso. Póngase en contacto con su administrador si cree que se trata de un error. Fecha de creación Credenciales Personalizar Filtro Borrar Describa el problema en detalle Deshabilitar Fecha de vencimiento Pendientes Editar Introduzca la contraseña, se recomiendan al menos 8 caracteres alfanuméricos con al menos una mayúscula. Expandir Exportar Descripción Completa Nombre completo Grupos Ayuda Historial Inicio Importar En Integraciones Inventario Facturación Incidencia Incidencias Etiquetas Ultima actualización Prospectos Salir Asegúrese de que la dirección sea correcta y de que cuenta con los permisos adecuados. Hace unos momentos Mi Nube Mi tablero Mi menú Mi perfil Nuevo Nueva Incidencia Nodos Abierto Operaciones Opcionalmente, una fecha de vencimiento. Anular Pagos Telefono Posiciona y dimensionar avatar Prioridad Perfil Perfiles Promociones Reporte Reportes Recurso no encontrado Ventas Buscar Incidencia Buscar Usuario Dentro de unos segundos Seleccione el estado de la cuenta. Seleccionar Imagen Seleccionar Meta Seleccione una o más cuentas si el problema las afecta. Seleccione uno o varios usuarios para asignar esta solicitud Seleccione el grupo al que pertenece la cuenta. Seleccione las etiquetas que mejor describan la situación de este incidente. Seleccione un peso para esta incidencia. Preferencias Cerrar la sesión Salir e iniciar con una cuenta diferente Parte de la información de esta cuenta ha sido actualizada Tareas El titulo de la Incidencia Esta cuenta ha sido importada con exito Titulo Mañana Inviables Peso Sin Metas Escriba el nombre real para que la gente que conoce pueda reconocerlo. Escriba el nombre de usuario, sera el credencial de acceso al sistema. SI Ayer Puede agregar una meta a esta incidencia. hace dia hora minuto mes traducido %s con parametros semana año 